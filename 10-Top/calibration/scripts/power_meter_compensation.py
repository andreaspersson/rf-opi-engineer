from org.csstudio.display.builder.runtime.script import PVUtil
import os
import subprocess
from os.path import expanduser

start           = PVUtil.getInt(pvs[4])    #loc://back_tocal

if start == 1:
    cbleatt = PVUtil.getDouble(pvs[0]) #cable_attenuation
    coupatt = PVUtil.getDouble(pvs[1]) #coupler_attenuation
    PPM     = PVUtil.getString(pvs[2]) #$(P_PM)
    RPM     = PVUtil.getString(pvs[3]) #$(R_PM)
    
    att = coupatt - cbleatt

    temp = PVUtil.createPV(PPM + ":" + RPM + ":offset_value",0)
    temp.write(att)
    PVUtil.releasePV(temp)

    temp = PVUtil.createPV(PPM + ":" + RPM + ":offset_switch",0)
    temp.write(1)
    PVUtil.releasePV(temp)

    # return to start parameter to 0
    pv_startsp = PVUtil.createPV("loc://back_tocal", 5000)
    pv_startsp.write(0)
    PVUtil.releasePV(pv_startsp)
