#!/usr/bin/env python3

import os
import sys
import numpy as np
from epics import PV, caget, caput
import time

os.environ["EPICS_CA_ADDR_LIST"] = "172.16.110.25 172.16.110.15 172.16.110.13 172.16.110.33 172.16.110.32"

SCB_pv = sys.argv[1]

data_read = 0

# Read SCB measurement
while data_read == 0:
    try:
        SCB_reading = caget(SCB_pv)
        time.sleep(0.1)
        data_read = 1
    except:
        data_read = 0

print(SCB_reading)
