#!/usr/bin/python3

import os
import sys
import time
import epics
import signal

# PVs
pv_nrpz_reset = epics.PV("rfcalib:RS_NRPZ-01-Ch0:SensorReset-S")

pv_nrpz_freqcorr = epics.PV("rfcalib:RS_NRPZ-01-Ch0:CorrFrequency-S")
pv_nrpz_freqcorr_rb = epics.PV("rfcalib:RS_NRPZ-01-Ch0:CorrFrequency-RBV")

pv_nrpz_offscorr = epics.PV("rfcalib:RS_NRPZ-01-Ch0:CorrOffset-S")
pv_nrpz_offscorr_rb = epics.PV("rfcalib:RS_NRPZ-01-Ch0:CorrOffset-RBV")
pv_nrpz_offsen = epics.PV("rfcalib:RS_NRPZ-01-Ch0:CorrOffsetEn-S")
pv_nrpz_offsen_rb = epics.PV("rfcalib:RS_NRPZ-01-Ch0:CorrOffsetEn-S")

pv_nrpz_trigsource = epics.PV("rfcalib:RS_NRPZ-01-Ch0:TrigSource-S")
pv_nrpz_trigsource_rb = epics.PV("rfcalib:RS_NRPZ-01-Ch0:TrigSource-RBV")

pv_nrpz_run = epics.PV("rfcalib:RS_NRPZ-01-Ch0:SensorAcquire-S")

pv_nrpz_value = epics.PV("rfcalib:RS_NRPZ-01-Ch0:MeasValueDbm-R")

# while loop 
def main():

    print("Starting NRPZ tester ...")
    pv_nrpz_run.put(0, wait=True)
    time.sleep(2)

    print("Send sensor reset")
    pv_nrpz_reset.put(1, wait=True)
    time.sleep(5)   

    print("offset enable")
    pv_nrpz_offsen.put(1, wait=True)

    while (pv_nrpz_offsen_rb.get() != 1):
        print("Checking offset enable readback")
        pv_nrpz_offsen.put(1, wait=True)
        time.sleep(0.5)

    print("Trigger source")
    pv_nrpz_trigsource.put(3, wait=True)
            
    while (pv_nrpz_trigsource_rb.get() != 3):
        print("Checking trigger source readback")
        time.sleep(0.5)


    step_type = 1

    while True:
        print("Send trigger OFF Power meter")
        pv_nrpz_run.put(0, wait=True)
        time.sleep(1)

        #print("Send sensor reset")
        #pv_nrpz_reset.put(1, wait=True)
        #time.sleep(1)   
        
        # Frequency correction
        print("Frequency correction")
        if ((step_type == 1) or (step_type == 2)):
            pv_nrpz_freqcorr.put(352210000.0, wait=True)
            
            while (pv_nrpz_freqcorr_rb.get() != 352209984.0):
                print("Checking freq. correction readback")
                time.sleep(0.5)
        else:
            pv_nrpz_freqcorr.put(704420000.0, wait=True)
            
            while (pv_nrpz_freqcorr_rb.get() != 704419968.0):
                print("Checking freq. correction readback")
                time.sleep(0.5)

        # Trigger source
        # print("Trigger source")
        # pv_nrpz_trigsource.put(3, wait=True)
            
        # while (pv_nrpz_trigsource_rb.get() != 3):
        #     print("Checking trigger source readback")
        #     time.sleep(0.5)

        # Offset correction
        print("offset correction")
        if ((step_type == 1) or (step_type == 3)):
            pv_nrpz_offscorr.put(29.5, wait=True)
            
            while (pv_nrpz_offscorr_rb.get() != 29.5):
                print("Checking offset correction readback")
                time.sleep(0.5)
        else:
            pv_nrpz_offscorr.put(33.49, wait=True)
            
            while ((pv_nrpz_offscorr_rb.get()) != 33.4900016784668):
                print("setpoint = 33.49")
                print("readback = ", pv_nrpz_offscorr_rb.get())
                time.sleep(0.5)

        # print("offset enable")
        # pv_nrpz_offsen.put(1, wait=True)
    
        # while (pv_nrpz_offsen_rb.get() != 1):
        #     print("Checking offset enable readback")
        #     pv_nrpz_offsen.put(1, wait=True)
        #     time.sleep(0.5)

        print("Send trigger ON Power meter")
        pv_nrpz_run.put(1, wait=True)
        time.sleep(1)

        print("Get 5 consecutive values")
        print("Offset correction enable = ", pv_nrpz_offsen_rb.get())
        print("Offset correction value  = ", pv_nrpz_offscorr_rb.get())
        print("Freq correction value    = ", pv_nrpz_freqcorr_rb.get())
        for i in range(5):
            val = pv_nrpz_value.get()
            print("Measurement ", i, " = ", val)
            time.sleep(1)

        # change parameters
        step_type += 1
        if (step_type == 5):
            step_type = 1


if __name__ == '__main__':
    main()
