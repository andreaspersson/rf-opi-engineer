#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

Kv = 107.8775565

KW = 1

filepath = input("Enter file path")
print(filepath)

data = np.genfromtxt(filepath,delimiter=',',skip_header=1,skip_footer=0)
print(data)

Y = data[:,0]
Y = np.reshape(Y,(len(Y),1))

if KW == 1:
    Y = Y*1000

rawcnt = data[:,1]
rawcnt = np.reshape(rawcnt,(len(rawcnt),1))

print(Y)
print(rawcnt)

A = np.square(rawcnt)

raw = np.arange(0,1.0005,0.0005)
raw = np.reshape(raw,(len(raw),1))

Yest = np.matmul( np.square(raw), np.matmul( np.matmul( np.linalg.inv( np.matmul( np.transpose(A),A ) ), np.transpose(A)), Y) )
print(Yest)

cav_volt = Kv*np.sqrt(Yest)

plt.plot(rawcnt,Y,marker=11)
plt.plot(raw,Yest)
plt.show()

plt.plot(raw,cav_volt)
plt.show()
