#!/usr/bin/env python3

import os
import sys
import numpy as np
from epics import PV, caget, caput
import time

os.environ["EPICS_CA_ADDR_LIST"] = "172.16.110.25 172.16.110.15 172.16.110.13 172.16.110.33 172.16.110.32 172.16.110.89"

PM_pv = sys.argv[1]
data_read = 0

# Read Power meter reading
while data_read == 0:
    try:
        pm_reading = caget(PM_pv)
        time.sleep(0.1)
        data_read = 1
    except:
        data_read = 0

print(pm_reading)
