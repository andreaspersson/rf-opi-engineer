<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>PIN Diodes Overview</name>
  <macros>
    <IOCSTATS_NAME>PIN Diode IOC</IOCSTATS_NAME>
    <R_LTC2991>:VMon-</R_LTC2991>
    <R_M24M02>:Eeprom-</R_M24M02>
    <R_TCA9555>:IOExp-</R_TCA9555>
    <R_TMP100>:Temp-</R_TMP100>
  </macros>
  <width>1610</width>
  <height>1230</height>
  <grid_visible>false</grid_visible>
  <widget type="rectangle" version="2.0.0">
    <name>title-bar</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>1610</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>title</name>
    <class>TITLE</class>
    <text>PIN Diode Overview</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>820</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_5</name>
    <class>SUBTITLE</class>
    <text></text>
    <x>760</x>
    <y use_class="true">20</y>
    <width>220</width>
    <height use_class="true">30</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <background_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </background_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">2</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="group" version="2.0.0">
    <name>SCL Group</name>
    <x>10</x>
    <y>340</y>
    <width>1600</width>
    <height>890</height>
    <style>3</style>
    <widget type="rectangle" version="2.0.0">
      <name>Rectangle</name>
      <width>1590</width>
      <height>890</height>
      <line_width>0</line_width>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Text Update_8</name>
      <text>SCL</text>
      <width>1590</width>
      <height>60</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="23.0">
        </font>
      </font>
      <foreground_color>
        <color name="HEADER-TEXT" red="0" green="0" blue="0">
        </color>
      </foreground_color>
      <background_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </background_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>$(pv_name)
$(pv_value)</tooltip>
    </widget>
    <widget type="group" version="2.0.0">
      <name>MBL Group</name>
      <macros>
        <SECTION>MBL</SECTION>
      </macros>
      <x>10</x>
      <y>60</y>
      <width>320</width>
      <height>680</height>
      <style>3</style>
      <widget type="rectangle" version="2.0.0">
        <name>Rectangle_6</name>
        <width>320</width>
        <height>680</height>
        <line_width>0</line_width>
        <background_color>
          <color name="Background" red="220" green="225" blue="221">
          </color>
        </background_color>
        <corner_width>10</corner_width>
        <corner_height>10</corner_height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-010RFC</name>
        <macros>
          <RFCELL>$(SECTION)-010RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>11</x>
        <y>40</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-020RFC</name>
        <macros>
          <RFCELL>$(SECTION)-020RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>11</x>
        <y>110</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-030RFC</name>
        <macros>
          <RFCELL>$(SECTION)-030RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>11</x>
        <y>180</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-040RFC</name>
        <macros>
          <RFCELL>$(SECTION)-040RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>11</x>
        <y>250</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-050RFC</name>
        <macros>
          <RFCELL>$(SECTION)-050RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>11</x>
        <y>320</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-060RFC</name>
        <macros>
          <RFCELL>$(SECTION)-060RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>11</x>
        <y>390</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-070RFC</name>
        <macros>
          <RFCELL>$(SECTION)-070RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>11</x>
        <y>460</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-080RFC</name>
        <macros>
          <RFCELL>$(SECTION)-080RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>11</x>
        <y>530</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-090RFC</name>
        <macros>
          <RFCELL>$(SECTION)-090RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>10</x>
        <y>600</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_9</name>
        <text>$(SECTION)</text>
        <width>310</width>
        <height>50</height>
        <font>
          <font family="Source Sans Pro" style="BOLD" size="21.0">
          </font>
        </font>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
    </widget>
    <widget type="group" version="2.0.0">
      <name>Spk Group</name>
      <macros>
        <SECTION>Spk</SECTION>
      </macros>
      <x>350</x>
      <y>60</y>
      <width>610</width>
      <height>540</height>
      <style>3</style>
      <widget type="rectangle" version="2.0.0">
        <name>Rectangle_7</name>
        <width>590</width>
        <height>540</height>
        <line_width>0</line_width>
        <background_color>
          <color name="Background" red="220" green="225" blue="221">
          </color>
        </background_color>
        <corner_width>10</corner_width>
        <corner_height>10</corner_height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-010RFC</name>
        <macros>
          <RFCELL>$(SECTION)-010RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/spk_station.bob</file>
        <x>10</x>
        <y>40</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-020RFC</name>
        <macros>
          <RFCELL>$(SECTION)-020RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/spk_station.bob</file>
        <x>10</x>
        <y>110</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-030RFC</name>
        <macros>
          <RFCELL>$(SECTION)-030RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/spk_station.bob</file>
        <x>10</x>
        <y>180</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-040RFC</name>
        <macros>
          <RFCELL>$(SECTION)-040RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/spk_station.bob</file>
        <x>10</x>
        <y>250</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-050RFC</name>
        <macros>
          <RFCELL>$(SECTION)-050RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/spk_station.bob</file>
        <x>10</x>
        <y>320</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-060RFC</name>
        <macros>
          <RFCELL>$(SECTION)-060RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/spk_station.bob</file>
        <x>10</x>
        <y>390</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-070RFC</name>
        <macros>
          <RFCELL>$(SECTION)-070RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/spk_station.bob</file>
        <x>10</x>
        <y>460</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-080RFC</name>
        <macros>
          <RFCELL>$(SECTION)-080RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/spk_station.bob</file>
        <x>300</x>
        <y>40</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-090RFC</name>
        <macros>
          <RFCELL>$(SECTION)-090RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/spk_station.bob</file>
        <x>300</x>
        <y>110</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-100RFC</name>
        <macros>
          <RFCELL>$(SECTION)-100RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/spk_station.bob</file>
        <x>300</x>
        <y>180</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-110RFC</name>
        <macros>
          <RFCELL>$(SECTION)-110RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/spk_station.bob</file>
        <x>300</x>
        <y>250</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_11</name>
        <text>$(SECTION)</text>
        <width>590</width>
        <height>50</height>
        <font>
          <font family="Source Sans Pro" style="BOLD" size="21.0">
          </font>
        </font>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-120RFC</name>
        <macros>
          <RFCELL>$(SECTION)-120RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/spk_station.bob</file>
        <x>300</x>
        <y>320</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-130RFC</name>
        <macros>
          <RFCELL>$(SECTION)-130RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/spk_station.bob</file>
        <x>300</x>
        <y>390</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
    </widget>
    <widget type="group" version="2.0.0">
      <name>HBL Group</name>
      <macros>
        <SECTION>HBL</SECTION>
      </macros>
      <x>980</x>
      <y>60</y>
      <width>590</width>
      <height>810</height>
      <style>3</style>
      <widget type="rectangle" version="2.0.0">
        <name>Rectangle_5</name>
        <width>590</width>
        <height>750</height>
        <line_width>0</line_width>
        <background_color>
          <color name="Background" red="220" green="225" blue="221">
          </color>
        </background_color>
        <corner_width>10</corner_width>
        <corner_height>10</corner_height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-010RFC</name>
        <macros>
          <RFCELL>$(SECTION)-010RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>10</x>
        <y>40</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-020RFC</name>
        <macros>
          <RFCELL>$(SECTION)-020RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>10</x>
        <y>110</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-030RFC</name>
        <macros>
          <RFCELL>$(SECTION)-030RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>10</x>
        <y>180</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-040RFC</name>
        <macros>
          <RFCELL>$(SECTION)-040RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>10</x>
        <y>250</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-050RFC</name>
        <macros>
          <RFCELL>$(SECTION)-050RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>10</x>
        <y>320</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-060RFC</name>
        <macros>
          <RFCELL>$(SECTION)-060RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>10</x>
        <y>390</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-070RFC</name>
        <macros>
          <RFCELL>$(SECTION)-070RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>10</x>
        <y>460</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-080RFC</name>
        <macros>
          <RFCELL>$(SECTION)-080RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>10</x>
        <y>530</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-090RFC</name>
        <macros>
          <RFCELL>$(SECTION)-090RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>10</x>
        <y>600</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-100RFC</name>
        <macros>
          <RFCELL>$(SECTION)-100RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>10</x>
        <y>670</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-110RFC</name>
        <macros>
          <RFCELL>$(SECTION)-110RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>10</x>
        <y>740</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-120RFC</name>
        <macros>
          <RFCELL>$(SECTION)-120RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>290</x>
        <y>40</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-130RFC</name>
        <macros>
          <RFCELL>$(SECTION)-130RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>290</x>
        <y>110</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-140RFC</name>
        <macros>
          <RFCELL>$(SECTION)-140RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>290</x>
        <y>180</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-150RFC</name>
        <macros>
          <RFCELL>$(SECTION)-150RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>290</x>
        <y>250</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-160RFC</name>
        <macros>
          <RFCELL>$(SECTION)-160RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>290</x>
        <y>320</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-170RFC</name>
        <macros>
          <RFCELL>$(SECTION)-170RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>290</x>
        <y>390</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-180RFC</name>
        <macros>
          <RFCELL>$(SECTION)-180RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>290</x>
        <y>460</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-190RFC</name>
        <macros>
          <RFCELL>$(SECTION)-190RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>290</x>
        <y>530</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-200RFC</name>
        <macros>
          <RFCELL>$(SECTION)-200RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>290</x>
        <y>600</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>Embedded $(SECTION)-210RFC</name>
        <macros>
          <RFCELL>$(SECTION)-210RFC</RFCELL>
        </macros>
        <file>../../99-Shared/pin-diode/rfc_station.bob</file>
        <x>290</x>
        <y>670</y>
        <width>290</width>
        <height>50</height>
        <resize>2</resize>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_10</name>
        <text>$(SECTION)</text>
        <width>590</width>
        <height>50</height>
        <font>
          <font family="Source Sans Pro" style="BOLD" size="21.0">
          </font>
        </font>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>NCL Group</name>
    <x>10</x>
    <y>60</y>
    <width>830</width>
    <height>260</height>
    <style>3</style>
    <widget type="rectangle" version="2.0.0">
      <name>Rectangle_2</name>
      <width>830</width>
      <height>260</height>
      <line_width>0</line_width>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="rectangle" version="2.0.0">
      <name>Rectangle_3</name>
      <x>21</x>
      <y>60</y>
      <width>790</width>
      <height>180</height>
      <line_width>0</line_width>
      <background_color>
        <color name="Background" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_18</name>
      <actions>
        <action type="open_display">
          <file>../../99-Shared/pin-diode/instance.bob</file>
          <macros>
            <IDX>1</IDX>
            <P>$(RFCELL):RFS-PIND-$(IDX)10</P>
            <RFCELL>RFQ-010</RFCELL>
          </macros>
          <target>window</target>
          <description>RFQ-010:RFS-PIND-110</description>
        </action>
      </actions>
      <text>Choose device</text>
      <x>101</x>
      <y>90</y>
      <width>159</width>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label</name>
      <text>RFQ</text>
      <x>41</x>
      <y>90</y>
      <width>50</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_3</name>
      <text>MEBT</text>
      <x>41</x>
      <y>139</y>
      <width>50</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_6</name>
      <text>DTL3</text>
      <x>301</x>
      <y>188</y>
      <width>50</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_7</name>
      <text>DTL4</text>
      <x>561</x>
      <y>90</y>
      <width>50</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_8</name>
      <text>DTL5</text>
      <x>561</x>
      <y>139</y>
      <width>50</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Text Update_9</name>
      <text>NCL</text>
      <width>830</width>
      <height>60</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="23.0">
        </font>
      </font>
      <foreground_color>
        <color name="HEADER-TEXT" red="0" green="0" blue="0">
        </color>
      </foreground_color>
      <background_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </background_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>$(pv_name)
$(pv_value)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_25</name>
      <actions>
        <action type="open_display">
          <file>../../99-Shared/pin-diode/instance.bob</file>
          <macros>
            <IDX>1</IDX>
            <P>$(RFCELL):RFS-PIND-$(IDX)10</P>
            <RFCELL>MEBT-010</RFCELL>
          </macros>
          <target>window</target>
          <description>MEBT-010:RFS-PIND-110</description>
        </action>
        <action type="open_display">
          <file>../../99-Shared/pin-diode/instance.bob</file>
          <macros>
            <IDX>2</IDX>
            <P>$(RFCELL):RFS-PIND-$(IDX)10</P>
            <RFCELL>MEBT-010</RFCELL>
          </macros>
          <target>window</target>
          <description>MEBT-010:RFS-PIND-210</description>
        </action>
        <action type="open_display">
          <file>../../99-Shared/pin-diode/instance.bob</file>
          <macros>
            <IDX>3</IDX>
            <P>$(RFCELL):RFS-PIND-$(IDX)10</P>
            <RFCELL>MEBT-010</RFCELL>
          </macros>
          <target>window</target>
          <description>MEBT-010:RFS-PIND-310</description>
        </action>
      </actions>
      <text>Choose device</text>
      <x>101</x>
      <y>139</y>
      <width>159</width>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_28</name>
      <actions>
        <action type="open_display">
          <file>../../99-Shared/pin-diode/instance.bob</file>
          <macros>
            <IDX>1</IDX>
            <P>$(RFCELL):RFS-PIND-$(IDX)10</P>
            <RFCELL>DTL-030</RFCELL>
          </macros>
          <target>window</target>
          <description>DTL-030:RFS-PIND-110</description>
        </action>
      </actions>
      <text>Choose device</text>
      <x>361</x>
      <y>188</y>
      <width>159</width>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_29</name>
      <actions>
        <action type="open_display">
          <file>../../99-Shared/pin-diode/instance.bob</file>
          <macros>
            <IDX>1</IDX>
            <P>$(RFCELL):RFS-PIND-$(IDX)10</P>
            <RFCELL>DTL-040</RFCELL>
          </macros>
          <target>window</target>
          <description>DTL-040:RFS-PIND-110</description>
        </action>
      </actions>
      <text>Choose device</text>
      <x>621</x>
      <y>90</y>
      <width>159</width>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_30</name>
      <actions>
        <action type="open_display">
          <file>../../99-Shared/pin-diode/instance.bob</file>
          <macros>
            <IDX>1</IDX>
            <P>$(RFCELL):RFS-PIND-$(IDX)10</P>
            <RFCELL>DTL-050</RFCELL>
          </macros>
          <target>window</target>
          <description>DTL-050:RFS-PIND-110</description>
        </action>
      </actions>
      <text>Choose device</text>
      <x>621</x>
      <y>139</y>
      <width>159</width>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_12</name>
      <text>DTL1</text>
      <x>301</x>
      <y>90</y>
      <width>50</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_13</name>
      <text>DTL2</text>
      <x>301</x>
      <y>139</y>
      <width>50</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_31</name>
      <actions>
        <action type="open_display">
          <file>../../99-Shared/pin-diode/instance.bob</file>
          <macros>
            <IDX>1</IDX>
            <P>$(RFCELL):RFS-PIND-$(IDX)10</P>
            <RFCELL>DTL-010</RFCELL>
          </macros>
          <target>window</target>
          <description>DTL-010:RFS-PIND-110</description>
        </action>
      </actions>
      <text>Choose device</text>
      <x>361</x>
      <y>90</y>
      <width>159</width>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_32</name>
      <actions>
        <action type="open_display">
          <file>../../99-Shared/pin-diode/instance.bob</file>
          <macros>
            <IDX>1</IDX>
            <P>$(RFCELL):RFS-PIND-$(IDX)10</P>
            <RFCELL>DTL-020</RFCELL>
          </macros>
          <target>window</target>
          <description>DTL-020:RFS-PIND-110</description>
        </action>
      </actions>
      <text>Choose device</text>
      <x>361</x>
      <y>139</y>
      <width>159</width>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Lab Group</name>
    <x>860</x>
    <y>60</y>
    <width>330</width>
    <height>260</height>
    <style>3</style>
    <widget type="rectangle" version="2.0.0">
      <name>Rectangle_1</name>
      <width>330</width>
      <height>260</height>
      <line_width>0</line_width>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Text Update_10</name>
      <text>Lab</text>
      <width>330</width>
      <height>60</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="23.0">
        </font>
      </font>
      <foreground_color>
        <color name="HEADER-TEXT" red="0" green="0" blue="0">
        </color>
      </foreground_color>
      <background_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </background_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>$(pv_name)
$(pv_value)</tooltip>
    </widget>
    <widget type="rectangle" version="2.0.0">
      <name>Rectangle_4</name>
      <x>20</x>
      <y>60</y>
      <width>290</width>
      <height>180</height>
      <line_width>0</line_width>
      <background_color>
        <color name="Background" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_1</name>
      <text>TS2</text>
      <x>30</x>
      <y>90</y>
      <width>80</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_2</name>
      <text>RFLAB</text>
      <x>30</x>
      <y>141</y>
      <width>80</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_20</name>
      <actions>
        <action type="open_display">
          <file>../../99-Shared/pin-diode/instance.bob</file>
          <macros>
            <IDX>1</IDX>
            <P>$(RFCELL):RFS-PIND-$(IDX)10</P>
            <RFCELL>LabS-RFLab</RFCELL>
          </macros>
          <target>window</target>
          <description>LabS-RFLab:RFS-PIND-110</description>
        </action>
      </actions>
      <text>Choose device</text>
      <x>120</x>
      <y>141</y>
      <width>159</width>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_26</name>
      <actions>
        <action type="open_display">
          <file>../../99-Shared/pin-diode/instance.bob</file>
          <macros>
            <IDX>1</IDX>
            <P>$(RFCELL):RFS-PIND-1$(IDX)0</P>
            <RFCELL>TS2-010RFC</RFCELL>
          </macros>
          <target>window</target>
          <description>TS2-010RFC:RFS-PIND-110</description>
        </action>
        <action type="open_display">
          <file>../../99-Shared/pin-diode/instance.bob</file>
          <macros>
            <IDX>1</IDX>
            <P>$(RFCELL):RFS-PIND-1$(IDX)0</P>
            <RFCELL>TS2-010RFC</RFCELL>
          </macros>
          <target>window</target>
          <description>TS2-010RFC:RFS-PIND-120</description>
        </action>
      </actions>
      <text>Choose device</text>
      <x>120</x>
      <y>90</y>
      <width>159</width>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
  </widget>
</display>
