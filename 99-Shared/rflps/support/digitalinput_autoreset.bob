<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>Auto-Reset Overview</name>
  <width>880</width>
  <height>22</height>
  <scripts>
    <script file="EmbeddedPy">
      <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

# AND operation to determine if pre-condition os OK
chenabled = PVUtil.getInt(pvs[2])
qout_status = PVUtil.getInt(pvs[1])

if (chenabled == 0):
	# if this channel is not needed, force precondition to OK
	pvs[0].write(1)
else:
	# if channel is enabled, pre-condition follows QOUT
	pvs[0].write(qout_status)
]]></text>
      <pv_name trigger="false">loc://$(PREFIX):$(DEVICE):HVONPreCondition</pv_name>
      <pv_name>$(PREFIX):$(DEVICE):FastIntStat</pv_name>
      <pv_name>$(PREFIX):$(DEVICE):IdleToHV-RB</pv_name>
    </script>
    <script file="EmbeddedPy">
      <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

# AND operation to determine if pre-condition os OK
chenabled = PVUtil.getInt(pvs[2])
qout_status = PVUtil.getInt(pvs[1])

if (chenabled == 0):
	# if this channel is not needed, force precondition to OK
	pvs[0].write(1)
else:
	# if channel is enabled, pre-condition follows QOUT
	pvs[0].write(qout_status)]]></text>
      <pv_name trigger="false">loc://$(PREFIX):$(DEVICE):RFONPreCondition</pv_name>
      <pv_name>$(PREFIX):$(DEVICE):FastIntStat</pv_name>
      <pv_name>$(PREFIX):$(DEVICE):HVToRF-RB</pv_name>
    </script>
  </scripts>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle</name>
    <width>880</width>
    <height>22</height>
    <line_width>1</line_width>
    <line_color>
      <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
      </color>
    </line_color>
    <background_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </background_color>
    <transparent>true</transparent>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_4</name>
    <pv_name>$(P)$(R)Desc</pv_name>
    <x>260</x>
    <y>1</y>
    <width>210</width>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="12.0">
      </font>
    </font>
    <transparent>true</transparent>
    <vertical_alignment>1</vertical_alignment>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_6</name>
    <text>$(P)$(R)</text>
    <x>50</x>
    <y>1</y>
    <width>200</width>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="12.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_9</name>
    <actions>
      <action type="open_display">
        <file>$(EXPERT_DISPLAY)</file>
        <target>tab</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>...</text>
    <x>530</x>
    <width>60</width>
    <height>20</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="14.0">
      </font>
    </font>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_8</name>
    <text>$(R_FIM)</text>
    <x>1</x>
    <y>1</y>
    <width>44</width>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="12.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="slide_button" version="2.0.0">
    <name>Slide Button_3</name>
    <pv_name>$(P)$(IOC_)$(R_FIM)-FastRst-En</pv_name>
    <label></label>
    <x>440</x>
    <width>50</width>
    <height>20</height>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_5</name>
    <pv_name>$(P)$(IOC_)$(R_FIM)-FastRst-IlckCnt</pv_name>
    <x>700</x>
    <y>1</y>
    <width>80</width>
    <font>
      <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="picture" version="2.0.0">
    <name>Picture</name>
    <file>images/interlock.svg</file>
    <x>630</x>
    <width>20</width>
    <height>20</height>
    <rules>
      <rule name="change_icon" prop_id="file" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>images/interlock tripped.svg</value>
        </exp>
        <pv_name>$(P)$(R)isFirstIlck</pv_name>
      </rule>
    </rules>
    <tooltip>Click to reset interlock</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_1</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Write PV</description>
      </action>
    </actions>
    <pv_name>$(P)$(R)IlckRst</pv_name>
    <text></text>
    <x>630</x>
    <width>20</width>
    <height>20</height>
    <background_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </background_color>
    <transparent>true</transparent>
    <rules>
      <rule name="enable if interlock" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>$(P)$(R)isFirstIlck</pv_name>
      </rule>
    </rules>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <enabled>false</enabled>
  </widget>
  <widget type="bool_button" version="2.0.0">
    <name>Boolean Button</name>
    <pv_name>$(P)$(IOC_)$(R_FIM)-FastRst-RstCnt</pv_name>
    <x>800</x>
    <y>1</y>
    <width>65</width>
    <height>20</height>
    <off_label>Reset</off_label>
    <on_label>Reset</on_label>
    <font>
      <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
      </font>
    </font>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
</display>
